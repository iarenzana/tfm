;; -*- coding: utf-8; -*-
;; Binder-Format-Version: 2
;; This is a Binder project file. It is meant to be human-readable, but you
;; probably shouldn't edit it.

(("README.org")
 ("bibtex-pdfs/")
 ("img")
 ("log.org")
 ("notes.org")
 ("references.bib")
 ("tfm_en.org"
  (notes . "Innovacion\nIntegration as a business\nDefine E9-1-1"))
 ("tfm_en.pdf")
 ("tfm_template.org"))
