#+OPTIONS: toc:nil date:nil
#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS: [a4paper, 12pt]
#+LATEX_HEADER: \usepackage[margin=1in]{geometry}
#+LATEX_HEADER: \usepackage{setspace}
#+LATEX_HEADER: \doublespacing
#+LATEX_HEADER: \usepackage{fancyhdr}
#+LATEX_HEADER: \fancyhead{}
#+LATEX_HEADER: \pagestyle{fancy}
#+LATEX_HEADER: \lfoot{}
#+LATEX_HEADER: \rfoot{}

#+LATEX_HEADER: \usepackage[citestyle=apa,bibstyle=apa,hyperref=true,backref=true, maxcitenames=3,url=true,backend=biber,natbib=true] {biblatex}
#+LATEX_HEADER: \addbibresource{references.bib}
#+LATEX_HEADER: \linespread{1.5}
#+LATEX_HEADER: \usepackage[T1]{fontenc}
#+LATEX_HEADER: \usepackage[utf8]{inputenc}
#+LATEX_HEADER: \usepackage{cantarell}
#+LATEX_HEADER: \usepackage{verbatim}
#+LATEX_COMPILER: pdflatex

#+BEGIN_COMMENT

#+LATEX_HEADER: \lhead{}


#+END_COMMENT
